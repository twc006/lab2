/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author twc006
 *
 */
public class Fahrenheit extends Temperature
{
  public Fahrenheit(float t)
  {
    super(t);
  }
  public String toString()
  {
    return Float.toString(this.getValue()) + " F";
  }
@Override
public Temperature toCelsius() {
	return new Celsius(((this.getValue()-32)*5)/9);
}
@Override
public Temperature toFahrenheit() {
	return new Fahrenheit(this.getValue());
}
@Override
public Temperature toKelvin() {
	return new Kelvin((float)((((this.getValue()-32)*5)/9)+273.15));
}
  
}
