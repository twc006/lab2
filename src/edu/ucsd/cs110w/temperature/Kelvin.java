/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (twc006): write class javadoc
 *
 * @author twc006
 */
public class Kelvin extends Temperature {

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	public Kelvin(float t)
	{
      super(t);
	}
	
	public String toString()
	{
      return Float.toString(this.getValue()) + " K";
	}
	
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius((float)(this.getValue()-273.15));
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit((float) ((this.getValue()-273.15)*1.8+32));
	}

	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Kelvin(this.getValue());
	}

}
