/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author twc006
 *
 */
public class Celsius extends Temperature
{
  public Celsius(float t)
  {
    super(t);
  }
  public String toString()
  {
    return Float.toString(this.getValue()) + " C";
  }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	return new Celsius(this.getValue());
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	return new Fahrenheit(((this.getValue()*9)/5)+32);
}
@Override
public Temperature toKelvin() {
	return new Kelvin((float)(this.getValue()+273.15));
}
}
